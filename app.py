from flask import Flask, jsonify

app = Flask(__name__)


@app.route("/api/get")
def get_data():
    return jsonify([{"name": "reza", "age": 25, "phone": "021"},
                    {"name": "ali", "age": 27, "phone": "023"},
                    {"name": "saeid", "age": 32, "phone": "025"}])


app.run(debug=True)
